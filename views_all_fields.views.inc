<?php

function views_all_fields_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_all_fields') . '/includes',
    ),
    'handlers' => array(
      'views_handler_field_views_all_fields' => array(
        'parent' => 'views_handler_field_custom',
      ),
    ),
  );
}

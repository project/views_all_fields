<?php

class views_handler_field_views_all_fields extends views_handler_field_custom {

  function option_definition() {
    $defaults = parent::option_definition();
    $defaults['vaf_fields_exclude'] = array('default' => array());
    $defaults['vaf_bundle'] = array('default' => array());
    $defaults['vaf_bundle_arg'] = array('default' => array());
    return $defaults;
  }

  /**
   * Options form to exclude fields.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['vaf_bundle'] = array(
      '#title' => t('Bundle'),
      '#description' => t('Pull fields from bundle. Leave blank for all. Resave to see allowed fields.'),
      '#type' => 'select',
      '#multiple' => TRUE,
      '#default_value' => $this->options['vaf_bundle'],
    );

    $form['vaf_bundle_arg'] = array(
      '#title' => t('Bundle argument'),
      '#description' => t('Use this contextual filter to set the bundle.'),
      '#type' => 'select',
      '#default_value' => $this->options['vaf_bundle_arg'],
    );

    foreach (entity_get_info() as $type => $info) {
      if ($info['base table'] == $this->table) {
        $entity_type = $type;
        foreach ($info['bundles'] as $key => $bundle_info) {
          $form['vaf_bundle']['#options'][$key] = $bundle_info['label'];
        }
        break;
      }
    }

    // Load handlers so we can access any configured contextual filters.
    $this->view->init_handlers();
    $cfilters = $this->view->argument;
    $form['vaf_bundle_arg']['#options'][''] = 'None';
    foreach ($cfilters as $key => $value) {
      $form['vaf_bundle_arg']['#options'][$key] = $key;
    }

    $entity_type_fields = field_info_instances($entity_type);
    $fields_in_view = $this->view->get_items('field');

    foreach ($entity_type_fields as $entity_type => $entity_instances) {
      foreach ($entity_instances as $field => $instance) {
        if (!isset($fields_in_view[$field]) && (empty($this->options['vaf_bundle']) || isset($this->options['vaf_bundle'][$instance['bundle']]))) {
          $options[$field] = "{$instance['label']} ({$instance['bundle']}-{$instance['field_name']})";
        }
      }
    }

    asort($options);
    $form['vaf_fields_exclude'] = array(
      '#title' => t('Exclude'),
      '#description' => t('Fields marked here will not be pulled. Note that fields already included in this view are already excluded.'),
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $this->options['vaf_fields_exclude'],
    );
  }

}
